function aufgabe20(k,s)
%%% Adaptives Schwellwertverfahren
%%% a) einfaches globales Schwellwertverfahren mit Hilfe von 'Graythresh'
%%% c) ortsabh�ngiger Schwellwert f�r inhomogenes Bild

% a) 
img_hom = dbv_imageRead('quadrat_homogen.png','gray');
img_inh = dbv_imageRead('quadrat_inhomogen.png','gray');

level_hom = graythresh(img_hom);            %% Schwellwert bestimmen via graythresh
level_inh = graythresh(img_inh);
BW_hom = im2bw(img_hom,level_hom);          %% Schwellwert anwenden 
BW_inh = im2bw(img_inh,level_inh);

figure(1)                                   %% darstellen
subplot(2,2,1)
imshow(img_hom)
subplot(2,2,2)
imshow(BW_hom)
subplot(2,2,3:4)
imhist(img_hom)
figure(2)
subplot(2,2,1)
imshow(img_inh)
subplot(2,2,2)
imshow(BW_inh)
subplot(2,2,3:4)
imhist(img_inh)

%%% c) 
%%% k x k -Nachbarschaft 
%%% s ist vorgegebener Schwellwert 
%%%  r ist Intervallbreite der [min,max] Graustufenwerte








