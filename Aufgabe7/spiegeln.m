function A_gespiegelt =  spiegeln(A)    % spiegeln von symmetrischen Matrizen
m = size(A,1);
Hilfsmatrix = zeros(m);
for i = 1: m 
    for j=1:m
    Hilfsmatrix(i,i) = A(m+1-i,m+1-i); 
    if i == j
    continue
    else
    Hilfsmatrix(i,j) = A(j,i);
    end 
    end
end 
A_gespiegelt = Hilfsmatrix';
