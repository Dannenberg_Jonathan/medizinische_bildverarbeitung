function aufgabe7(filename1, filename2)

imgB1 = dbv_imageRead(filename1, 'gray');
imgB2 = dbv_imageRead(filename2, 'gray');

%zuerst einmal Beide Bilder zeigen 
figure(1)
subplot(1,2,1)
imshow(imgB1)
subplot(1,2,2)
imshow(imgB2)
%Spektrum dezentrieren und invers Fourier-transformieren
%Da das Spektrum reellwertig ist, ist die IFT komplexwertig, daher
%Benutzung des Realteiles der IFT.
figure(2)
subplot(1,2,1)
imgB1_dez = ifftshift(imgB1);               %Dezentrieren
imgB1_ifft = real(ifft2(imgB1_dez));        %invers Transformieren, nur Realteil
%Bild skalieren auf 0-255 (daf�r braucht man uint8 )
%zu Fu�: 
 imgB1_ifft =uint8( imgB1_ifft/max(imgB1_ifft(:))*255); 
    imshow(imgB1_ifft)                       %Bild zeigen
% Matlab skaliert: 
%imshow(imgB1_ifft,[])                       %MATLAB Konvention: double-Bilder Wertebereich[0,1]; uint8[0,255]
                        
subplot(1,2,2)
imgB2_dez = ifftshift(imgB2);
imgB2_ifft = real(ifft2(imgB2_dez));
imshow(imgB2_ifft,[])


%%%%% au�en liegen h�here Frequenzen, was in den Bildern sichtbar ist. 
%%%%  die 'Richtung' der Welle wird durch die Lage der Punkte im
%%%%  Ortsfrequenzbereich bestimmt.
%%%%%% 
%b)
figure(3)
imgB3_dez = ifftshift(imgB1+imgB2);
imgB3_ifft = real(ifft2(imgB3_dez));
imshow(imgB3_ifft,[])


%% c) Die Lage verschiebt sich. vgl b) Wird die Welle1 90� gedreht, so verscheiben sich die Punkte in im Ortsfrequenzbereich zu denen von Welle2 



