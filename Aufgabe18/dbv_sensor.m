function [Fmod, H] = dbv_sensor(F, s, v)
%[Fmod, H] = dbv_sensor(F, s, v)

%�bertragungsfkt als Gewichtsfkt direkt im Frequenzbereich erstellen
winsize = size(F);
h = fspecial('gaussian', min(winsize), 2*s+1);   %Gewichtsfkt
c = fix(winsize/2);                     %Mittelpunkt des Bildes
r = fix(size(h)/2);                     %Bereich>0 um den Mittelpunkt
H = zeros(size(F));                   %erweiterte Gewichtsfkt
H(c(1)-r(1)+1:c(1)+r(1), c(2)-r(2)+1:c(2)+r(2)) = h;
H = fftshift(H);                        %Gewichtsfkt dezentrieren
H = H / max(max(H));

imgfiltered = ifft2(F .* H, 'symmetric');
%Rauschen hinzuf�gen
imgfiltered = uint8(imgfiltered);
imgfiltered = dbv_addNoise(imgfiltered, 'gaussian', v);

%R�ckgabe des Bildes im Frequenzbereich
Fmod = fft2(double(imgfiltered));