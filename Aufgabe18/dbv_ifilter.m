function inversgefiltert = dbv_ifilter(F_mod,H,S)       % Funktion gibt inversgefiltertes Signal zur�ck. F_mod ist im k-Raum
inversgefiltert = zeros(size(F_mod));                   % initialisiere Ausgabematrix bzw Bild
for i = 1:size(F_mod,1)                                 %durchlaufe gesamtes Bild
    for j = 1:size(F_mod,2)                             % reihen und spaltenweise    

if abs(1/H(i,j)) < S                                    % Fallunterscheideung der in der Afgabenstellung gegebene Fkt
    inversgefiltert(i,j) = F_mod(i,j) / H(i,j);         % elementweise F/H bzw.
else 
    inversgefiltert(i,j) = F_mod(i,j) * S;              % F*S
end 

    end 
end 