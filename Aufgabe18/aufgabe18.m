function aufgabe18(filename)
%%% 1. Funktion dbv_ifilter implementieren
%%% 2. dbv_sensor auf buecherregal.png anwenden mit s=40 und v=1/1000
%%% 3. Originalbild, Sensorergebnis-Bild, und restauriertes Bild darstellen

%%% 1. in separater m-File
%%% 2. 
img = dbv_imageRead(filename,'gray');
img = double(img);
[F_mod,H] = dbv_sensor(fft2(img),40,0.001);      %% H aus F_mod = HF + R eine art Tiefpassfilter  [s bestimmt Gr��e und v Varianz]
%F_mod ist im OrtsFreqRaum                                                 %% fft2 damit img im OrtsFreqRaum ist (braucht dbv_sensor)
figure(1)
imshow(img,[])
title('Originalbild')
figure(2)
imshow(ifft2(F_mod),[])                         %% ifft2 um Bild anzuzeigen
title('Sensorergebnis')
%%% Bild restaurieren

Restauriert = dbv_ifilter(F_mod,H,2);           % wende Restauration an (s. dbv_ifilter)
figure(3)
imshow(ifft2(Restauriert),[])                   % retauriertes Bild anzeigen ifft2 um es in Ortsbereich zu holen
title('restauriertes Bild')


