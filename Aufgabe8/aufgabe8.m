function aufgabe8(filename)

img = dbv_imageRead(filename, 'gray');
img = double(img);

%Aufgabenteil a
%2D FT berechnen, Normierung und Zentrierung des Spektrums
img_fft = fft2(img);                          % 2D-FFT
img_fft_shifted = fftshift(img_fft);          %  zentrieren der tiefsten Frequenz in die Mitte des Ortsfrequenzraums
amp = abs(img_fft_shifted);
%phi = atan(imag(img_fft_shifted)/real(img_fft_shifted));
phi = angle(img_fft_shifted);                 % angle macht im Prinzip dasselbe wie arctan(Im/Re), aber der Bereich ist von -pi;pi und nicht -pi/2;pi/2
figure(1)
subplot(1,2,1)
imshow(log(amp),[])                           % log (nur) f�r sch�nere Darstellung
subplot(1,2,2)
imshow(phi,[])

%Spektrum setzt sich zusammen aus:   img_fft_shifted = amp .* exp(1i .* phi)

%Aufgabenteil b
%Amplitude auf 'null' bzw. const Wert (hier 1) [bei null w�re das BIld
%schwarz]
Amplitude_const = exp(1i.*phi);
Amplitude_const_dezentriert = ifftshift(Amplitude_const);  % Dezentrieren nicht vergessen -.-
figure(2)
imshow(ifft2(Amplitude_const_dezentriert),[])               %Darstellung der R�cktrafo 
%Phase auf null (diesmal wirklich)
Phase_null = amp;
Phase_null_dezentriert = ifftshift(Phase_null);             %analog
figure(3)
imshow(log(ifft2(Phase_null_dezentriert)),[])

