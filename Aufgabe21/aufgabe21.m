function aufgabe21(filename,min,max)
img = dbv_imageRead(filename, 'gray');
BW = edge(img);                             % Sobel-Operator
figure(1)
imshow(BW,[])
[B,L] = bwboundaries(BW,'noholes'); % B is a P-by-1 cell array, where P is the number of objects 
   % and holes. Each cell contains a Q-by-2 matrix, where Q is the number of
   % boundary pixels for the corresponding region. (aus der doc)
Konturlaengen = zeros(1,size(B,1));             %Initialisiere Vektor, der die L�ngen speichert
for i=1:size(B,1)
Konturlaengen(i) = size(B{i},1);                % Greife auf die jeweilige Kantenl�nge zu
end 
%bar(1:size(B,1),Konturlaengen)                  % Stelle dies in einem barplot dar.
%xlabel('Konturindex ')
%ylabel('Konturl�nge')

hist(Konturlaengen,50)
xlabel('Konturl�nge')
ylabel('H�ufigkeit')

%%% b) Konturen im Originalbild farblich markieren
figure(2) 
clf
imshow(img,[])
hold on                                     % Plots werden alle in einer figure gezeichnet 
for i=1:size(B,1)                           % Wiederhole f�r alle Kanten
Kanten = B{i,1};                            % Zugriff auf die Polygonz�ge, welche im CA gespeichert sind   
plot(Kanten(:,2),Kanten(:,1),'b')           % Koordinaten der Polygonz�ge gegeneinander auftragen
end 

%%% c) Min und MAx. Konturenl�nge soll ber�cksichtigt werden
%%% b) Konturen im Originalbild farblich markieren
figure(3) 
clf
imshow(img,[])
hold on
for j=1:size(B,1)
   if Konturlaengen(j) < min,           %gleicher Code wie b) mit Fallunterscheidung
       continue
   elseif Konturlaengen(j) > max,
       continue
   else
        Kanten = B{j,1};
        plot(Kanten(:,2),Kanten(:,1),'b')
   end 
end 