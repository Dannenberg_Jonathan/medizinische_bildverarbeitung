function aufgabe26()
addpath 'E:\UNI\Master\Semester 1\Medizinische Bildverarbeitung\�bung\dbv1516\bilder\bilderPCA'
Bilder = dir('E:\UNI\Master\Semester 1\Medizinische Bildverarbeitung\�bung\dbv1516\bilder\bilderPCA');
[w,h] = size(dbv_imageRead(Bilder(3).name,'gray'));
Datenmatrix = zeros(14,w*h);
for i = 3:16,
    img = dbv_imageRead(Bilder(i).name,'gray');
    Bildvektor = zeros(1,w*h);
    img = img';                     %Transponieren um zeilenweise auszulesen (ginge auch spaltenweise)
    Bildvektor(1:end) = img(1:end); % Bildvektor ist ein Zeilenvektor mit allen Bildinformationen
    Datenmatrix(i-2,:) = Bildvektor;
end 

[evectors,score,evalues] = princomp(Datenmatrix);
%evalues = sort(evalues,'descend');
figure(1)
bar(1:20,evalues(1:20))
xlabel('Rang')
ylabel('Eigenwert')

%%% Eigenvektoren darstellen 
figure(2) 
for i=1:3
ev = evectors(:,i);
img_ev = reshape(ev,69,69);
subplot(1,3,i)
imshow(img_ev,[])
title(['Eigenvektor ',num2str(i)])
end

%%% Transformation
D_strich = zeros(14,3);
D_strich(:,1:3) = score(:,1:3);


%%% c) 
%%% Mittelwert der Gruppen
%%% erste acht bilder sind schrauben
Prototyp_Schraube_x = 1/8 * sum(D_strich(1:8,1));
Prototyp_Schraube_y = 1/8 * sum(D_strich(1:8,2));
Prototyp_Schraube_z = 1/8 * sum(D_strich(1:8,3));
%%% letzte 6 bilder sind muttern 
Prototyp_Muttern_x = 1/6 * sum(D_strich(9:14,1));
Prototyp_Muttern_y = 1/6 * sum(D_strich(9:14,2));
Prototyp_Muttern_z = 1/6 * sum(D_strich(9:14,3));
%%%Darstellung der 14 Bildpunkte in 3D-Plot
figure(3) 
plot3(D_strich(:,1),D_strich(:,2),D_strich(:,3),'b.')
hold on 
plot3(Prototyp_Schraube_x,Prototyp_Schraube_y,Prototyp_Schraube_z,'r.')
plot3(Prototyp_Muttern_x,Prototyp_Muttern_y,Prototyp_Muttern_z,'g.')

%%% Zuordnung zur Gruppe 
Schraube = [Prototyp_Schraube_x,Prototyp_Schraube_y,Prototyp_Schraube_z];
Mutter = [Prototyp_Muttern_x,Prototyp_Muttern_y,Prototyp_Muttern_z];
figure(4)
plot3(Prototyp_Schraube_x,Prototyp_Schraube_y,Prototyp_Schraube_z,'kx')
hold on
plot3(Prototyp_Muttern_x,Prototyp_Muttern_y,Prototyp_Muttern_z,'ko')
for i=1:14
    vec = D_strich(i,1:3);    
    Abstand_Schraube = norm((vec-Schraube),2);
    Abstand_Mutter = norm((vec-Mutter),2);
    if Abstand_Schraube <= Abstand_Mutter
        Gruppe = 'Schraube';
        plot3(D_strich(i,1),D_strich(i,2),D_strich(i,3),'r.')
        fprintf('Dateiname:%s \t Gruppe:%s \n',Bilder(i+2).name,Gruppe);
    else
        Gruppe = 'Mutter';  
        plot3(D_strich(i,1),D_strich(i,2),D_strich(i,3),'g.')
        fprintf('Dateiname:%s \t Gruppe:%s \n',Bilder(i+2).name,Gruppe);
    end 
end 
legend('Schraube','Mutter')
%%% d) siehe aufgabe26_2dim bzw -1dim
%%% e)
%%% Basisvektoren an der iten Stelle ungleich 0 
%%% Eigenvektoren an 'allen' Komponenten ungleich null enthalten mehr
%%% Bildinformation



