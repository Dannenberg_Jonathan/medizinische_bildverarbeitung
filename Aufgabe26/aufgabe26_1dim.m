function aufgabe26_1dim()
addpath 'E:\UNI\Master\Semester 1\Medizinische Bildverarbeitung\�bung\dbv1516\bilder\bilderPCA'
Bilder = dir('E:\UNI\Master\Semester 1\Medizinische Bildverarbeitung\�bung\dbv1516\bilder\bilderPCA');
[w,h] = size(dbv_imageRead(Bilder(3).name,'gray'));
Datenmatrix = zeros(14,w*h);
for i = 3:16,
    img = dbv_imageRead(Bilder(i).name,'gray');
    Bildvektor = zeros(1,w*h);
    img = img';                     %Transponieren um zeilenweise auszulesen (ginge auch spaltenweise)
    Bildvektor(1:end) = img(1:end); % Bildvektor ist ein Zeilenvektor mit allen Bildinformationen
    Datenmatrix(i-2,:) = Bildvektor;
end 

[evectors,score,evalues] = princomp(Datenmatrix);
%evalues = sort(evalues,'descend');
figure(1)
bar(1:20,evalues(1:20))
xlabel('Rang')
ylabel('Eigenwert')

%%% Eigenvektoren darstellen 
figure(2) 

ev = evectors(:,1);
img_ev = reshape(ev,69,69);
imshow(img_ev,[])
title(['Eigenvektor ',num2str(1)])


%%% Transformation
D_strich = zeros(14,1);
D_strich(:,1) = score(:,1);


%%% c) 
%%% Mittelwert der Gruppen
%%% erste acht bilder sind schrauben
Prototyp_Schraube_x = 1/8 * sum(D_strich(1:8,1));
%Prototyp_Schraube_y = 1/8 * sum(D_strich(1:8,2));
%Prototyp_Schraube_z = 1/8 * sum(D_strich(1:8,3));
%%% letzte 6 bilder sind muttern 
Prototyp_Muttern_x = 1/6 * sum(D_strich(9:14,1));
%Prototyp_Muttern_y = 1/6 * sum(D_strich(9:14,2));
%Prototyp_Muttern_z = 1/6 * sum(D_strich(9:14,3));
%%%Darstellung der 14 Bildpunkte in 3D-Plot
figure(3) 
plot(D_strich(:,1),'b.')
hold on 
plot(Prototyp_Schraube_x,'r.')
plot(Prototyp_Muttern_x,'g.')

%%% Zuordnung zur Gruppe 
Schraube = Prototyp_Schraube_x;
Mutter = Prototyp_Muttern_x;
figure(4)
plot(Prototyp_Schraube_x,'kx')
hold on
plot(Prototyp_Muttern_x,'ko')
for i=1:14
    vec = D_strich(i,1);    
    Abstand_Schraube = norm((vec-Schraube),2);
    Abstand_Mutter = norm((vec-Mutter),2);
    if Abstand_Schraube <= Abstand_Mutter
         plot(D_strich(i,1),'r.')
        fprintf('Dateiname:%s \t Gruppe:Schraube \n',Bilder(i+2).name);
    else
        Gruppe = 'Mutter';  
        plot(D_strich(i,1),'g.')
        fprintf('Dateiname:%s \t Gruppe:%s \n',Bilder(i+2).name,Gruppe);
    end 
end 
legend('Schraube','Mutter')




