function Y = meineFFT(X)  % FFT f�r Vektoren 
N=length(X);
Y=zeros(N,1);
for k=1:N,
    for n=1:N
            Y(k)=Y(k)+ X(n) *exp(-2i*pi*(k-1)*(n-1)/N);
    end
end 