function aufgabe24(filename)
%%% Fourierdeskriptoren, Buchstabenerkennung 
%%% Approximation von Objekten
% 1. Liste L von Kunturpunkten (x_i,y_i) in Liste C �berf�hren welche aus kompl Zahlen x +iy besteht
% 2. 1D FFT von Liste C in Liste D [n/2 paare (-i,i) und 0 als Mittelwert]
% 3. iFFT von Liste D in Liste C' dabei wird M<(n/2) ber�cksichtigt 
% 4. aus C' wird L' bestimmt x=Re, y=Im 

% 1. Liste L von Kunturpunkten (x_i,y_i) in Liste C �berf�hren welche aus kompl Zahlen x +iy besteht
img = dbv_imageRead(filename,'gray');
BW = edge(img,'sobel','nothinning');                             % analog zu Aufgabe21
figure(1)
imshow(BW,[])
[B,L] = bwboundaries(BW,'noholes');
%L_strich = dbv_fourierDeskriptoren(B,M);

figure(2) 
clf
imshow(img,[])
hold on                                     % Plots werden alle in einer figure gezeichnet 
for i=1:size(B,1)                           % Wiederhole f�r alle Kanten
Kanten = B{i,1};                            % Zugriff auf die Polygonz�ge, welche im CA gespeichert sind   
plot(Kanten(:,2),Kanten(:,1),'b','LineWidth',2)           % Koordinaten der Polygonz�ge gegeneinander auftragen
end 

figure(3) 
clf
for k=1:16
    M=k;
    subplot(4,4,k)
    imshow(img,[])
    hold on
    L_strich = dbv_fourierDeskriptoren(B,M);
    for i=1:size(L_strich,1)                           % Wiederhole f�r alle Kanten
    Kanten = L_strich{i,1};                            % Zugriff auf die Polygonz�ge, welche im CA gespeichert sind   
    plot(Kanten(:,1),Kanten(:,2),'r','LineWidth',2) 
    end                                                         % Koordinaten der Polygonz�ge gegeneinander auftragen
end 


