function  L_strich = dbv_fourierDeskriptoren(B,M) %B enth�lt Polgonz�ge/Konturen aus einem Bild (bwboundaries)
% 1. Liste L von Kunturpunkten (x_i,y_i) in Liste C �berf�hren welche aus kompl Zahlen x +iy besteht
% 2. 1D FFT von Liste C in Liste D [n/2 paare (-i,i) und 0 als Mittelwert]
% 3. iFFT von Liste D in Liste C' dabei wird M<(n/2) ber�cksichtigt 
% 4. aus C' wird L' bestimmt x=Re, y=Im 

% 1. Liste L von Kunturpunkten (x_i,y_i) in Liste C �berf�hren welche aus kompl Zahlen x +iy besteht
L_strich = cell(size(B,1),1);
for j=1:size(B,1)                           % Wiederhole f�r alle Kanten
Kanten = B{j,1};                            % Zugriff auf die Polygonz�ge, welche im CA gespeichert sind 
% Liste L
x=Kanten(:,2);                              % erzeugen der (x,y)Koordinaten der Objekte
y=Kanten(:,1);
N=size(Kanten,1);
C=x+1i*y;
% 2. 1D FFT von Liste C in Liste D [n/2 paare (-i,i) und 0 als Mittelwert]
D = fft(C);
%D=D(2:(N+1)/2);    % D(1) entspricht Mittenfrequenz, D(2)=D*(N) (compl conj); D(k)=D*(N-k+2) [in MAtlab +1 f�r index +1 f�r mittenfr]
% 3. iFFT von Liste D in Liste C' dabei wird M<(n/2) ber�cksichtigt 
D=[D(1);D(2:1+M);zeros(N-1-2*M,1);D(end-M+1:end)];
C_strich = ifft(D);
%C_strich = [C_strich(1);C_strich(2:1+M);zeros(N-1-2*M,1);flipud(C_strich(2:1+M))];
% 4. aus C' wird L' bestimmt x'=Re, y'=Im 
L_strich{j} = [real(C_strich),imag(C_strich)];
end 
