function Aufgabe13(filename)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

img = dbv_imageRead(filename, 'gray'); %Bild wird eingelesen
bits = dbv_huffmanCodeLength(uint8(img));
%Originalbild anzeigen
figure(1)
subplot(2,2,1)
imshow(filename)
title('B�cherregal Originalbild')
%Graustufenbild anzeigen
subplot(2,2,2)
imshow(uint8(img))
title('B�cherregal Graustufenbild')    
%Histogramm anzeigen
subplot(2,2,3)
imhist(uint8(img))
ylabel('H�ufigkeit')
xlabel('Grauwert')
title('Grauwerthistogramm')
%Huffman-Code-Statistik anzeigen
subplot(2,2,4)            %Man sieht, dass h�ufig vorkommende Grauwerte mit weniger bits codiert werden und diejenigen die gar nciht vorkommen mit vielen bits gespeichert werden
bar(bits,'b')
ylabel('bit')
xlabel('Graustufe')
title('Huffman-Codestatistik')

%%% Ben�tigte bits zum Speichern von 'ast.png' bei Huffmann-Codierung:
[Haeufigkeit,Grauwert] = imhist(img);
Speicher_Huff = 0;
Speicher_nichtKomprimiert = 0;
for i=1:256
Speicher_Huff = Speicher_Huff + Haeufigkeit(i) * bits(i);   
Speicher_nichtKomprimiert = Speicher_nichtKomprimiert + Haeufigkeit(i) * 8; % man ben�tigt bei 'naiver' Codierung 8 bit pro Grauwert
end
Speicher_nichtKomprimiert_KB = Speicher_nichtKomprimiert/8/1024;%in KB
Speicher_Huff_KB = Speicher_Huff/8/1024;
Speicher_diff_KB = (Speicher_nichtKomprimiert - Speicher_Huff)/8/1024;
fprintf('\n Das Bild ben�tigte bei nicht komprimierter Kodierung einen Speicher von %g [KB]. \n Bei Huffman-Kodierung sind es %g [KB]. \n Dadurch werden %g[KB] Speicherplatz gespart.\n ',Speicher_nichtKomprimiert_KB,Speicher_Huff_KB,Speicher_diff_KB)


%b)
%%%%% Erzeuge Rauschen und lege es �ber das Bild, bstelle Histogramm und Huffmann-Codestatistik dar und berechne Speicher                                    
% Bilder mit Rauschen darstelllen
for i = 1:4                                             % vier unterschiedliche Standardabweichungen
    figure(2)    
    subplot(3,2,1:2)
	imshow(uint8(img))
    title('B�cherregal Originalbild')                              
    sigma_noise = 1/1000*(i).^3;                      % Legt die Standardabweichung fest
    imgNoise = dbv_addNoise(img, 'gaussian', sigma_noise); % erzeuge verrauschtes Bild
    subplot(3,2,i+2)                             
    imshow(imgNoise)
    title(['Rauschen;\sigma=' num2str(1/1000*(i).^3)])        
end
% Histogramme
for i = 1:4                                   % vier unterschiedliche Standardabweichungen
    figure(3)                           
    subplot(3,2,1:2)
    imhist(uint8(img))
    title('Grauwerthistogramm')
    sigma_noise = 1/1000*i.^3;                      % Legt die Standardabweichung fest
    imgNoise = dbv_addNoise(img, 'gaussian', sigma_noise); % erzeuge verrauschtes Bild
    subplot(3,2,i+2)                             
    imhist(uint8(imgNoise))
    title(['Rauschen;\sigma=' num2str(1/1000*i.^3)])   % Titel dient der �bersicht  
end
%Huffmann-Codestatistik
% & berechnen der Speichergr��e
for i = 1:4                                    % vier unterschiedliche Standardabweichungen
    figure(4)                     
    subplot(3,2,1:2)
    bits = dbv_huffmanCodeLength(uint8(img));
    bar(bits,'b')
    title('Huffman-Codestatistik')
    sigma_noise = 1/1000*i.^3;                      % Legt die Standardabweichung fest
    imgNoise = dbv_addNoise(img, 'gaussian', sigma_noise); % erzeuge verrauschtes Bild
    bits_noise = dbv_huffmanCodeLength(uint8(imgNoise));
    %%% Ben�tigte bits zum Speichern von 'ast.png' bei Huffmann-Codierung:
[Haeufigkeit,Grauwert] = imhist(imgNoise);
Speicher_Huff = 0;
for k=1:256
Speicher_Huff = Speicher_Huff + Haeufigkeit(k) * bits_noise(k);   
end
Speicher_Huff_KB = Speicher_Huff/8/1024;
Speicher_diff_KB = (Speicher_nichtKomprimiert - Speicher_Huff)/8/1024;
string = sprintf('Bei einem Rauschen von: %g',sigma_noise);
fprintf('\n %s \n Das Bild ben�tigte bei nicht komprimierter Kodierung einen Speicher von %g [KB]. \n Bei Huffman-Kodierung sind es %g [KB]. \n Dadurch werden %g[KB] Speicherplatz gespart.\n ',string,Speicher_nichtKomprimiert_KB,Speicher_Huff_KB,Speicher_diff_KB)
    
    
    subplot(3,2,i+2)                              

    bar(bits_noise,'b')
    title(['Rauschen;\sigma=' num2str(1/1000*i.^3)])
end
end

