function aufgabe23(filename)
img = dbv_imageRead(filename,'gray');
BW = edge(img);                             % analog zu Aufgabe 21
figure(1)
imshow(BW,[])
[B,L] = bwboundaries(BW,'noholes');

figure(2) 
Flaecheninhalte = zeros(size(B,1),1);       %initialisiere Vektor in welchem A, bzw U gespeichert wird
Umfang = zeros(size(B,1),1);
Kreishaftigkeit = zeros(size(B,1),1);
clf
imshow(img,[])
hold on                                     % Plots werden alle in einer figure gezeichnet 
for i=1:size(B,1)                           % Wiederhole f�r alle Kanten
Kanten = B{i,1};                            % Zugriff auf die Polygonz�ge, welche im CA gespeichert sind  
x= Kanten(:,2);                             % der �bersicht halber werden x,y und N eingef�hrt [intuitiv w�re x bei :,1 und nicht bei :,2]
y= Kanten(:,1);
N=size(Kanten,1);
%plot(x,y,'b')           % Koordinaten der Polygonz�ge gegeneinander auftragen
for j = 1:N-1,
    Flaecheninhalte(i,1) = Flaecheninhalte(i,1) + x(j)*y(j+1) - x(j+1)*y(j);  %Gau�sche Trapezformel f�r A eines Polygon
    Umfang(i,1)          = Umfang(i,1)          + ((x(j+1)-x(j))^2 + (y(j+1)-y(j))^2)^0.5; %aus Satz d Pythagoras
end 
Flaecheninhalte(i,1) = (Flaecheninhalte(i,1) +  x(N)*y(1) - x(1)*y(N))/2;       %N-te Element bereitet sonst Probleme und wird aus der Summer herausgezogen A muss noch halbiert werden
Umfang(i,1) =           Umfang(i,1)          +  ((x(1)-x(N))^2 + (y(1)-y(N))^2)^0.5;
Kreishaftigkeit(i,1) = 4*pi*Flaecheninhalte(i,1)/Umfang(i,1)^2;  %Aufgabenteil b) K = 4* pi* A / U^2
if Kreishaftigkeit(i,1) > 0.75,
    plot(x,y,'g')
else 
    plot(x,y,'r')
end 
end 
disp('Die Fl�cheninhalte der Figuren betragen:[pixel^2]')
disp(Flaecheninhalte)
disp('Die Umf�nge der Figuren betragen:[pixel]')
disp(Umfang)                                                        % Darstellung der Ergebnisse
disp('Die Kreishaftigkeiten der Figuren betragen:')
disp(Kreishaftigkeit)

figure(3) 
bar(Kreishaftigkeit)



