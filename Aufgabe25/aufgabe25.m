function aufgabe25()
addpath 'E:\UNI\Master\Semester 1\Medizinische Bildverarbeitung\�bung\dbv1516\bilder\bilderFM'
%a)
figure(1)
Bilder = dir('E:\UNI\Master\Semester 1\Medizinische Bildverarbeitung\�bung\dbv1516\bilder\bilderFM');
%dir erzeugt ein SA welches die Dateinamen(ua.) enth�lt; es werden au�erdem
%'.' und '..' ausgegeben, welche hier nicht notwendig sind aber deshalb von
%3:14 und nicht 1:12
for i = 3:14,
    img = dbv_imageRead(Bilder(i).name,'gray'); %Bilder(i).name erlaubt Zugriff auf den i-ten Dateinamen
    img = edge(img,'sobel','nothinning');
    subplot(4,3,i-2)
    imshow(img,[])
    title(num2str(i-2))
end 

%b) und c)
X=zeros(12,2);
figure(2)
glaette7 = strel('square',7);
for i = 3:14,
    img = dbv_imageRead(Bilder(i).name,'gray'); %Bilder(i).name erlaubt Zugriff auf den i-ten Dateinamen
    img = edge(img,'sobel','nothinning');
    img = imclose(img,glaette7);
    [B,L] = bwboundaries(img,'noholes');
    Konturlaenge = zeros(1,size(B,1));             %Initialisiere Vektor, der die L�ngen speichert
    for j=1:size(B,1)
        Konturlaenge(j) = size(B{j},1);                % Greife auf die jeweilige Kantenl�nge zu
    end  
    Konturmax = max(Konturlaenge);
    k = find(Konturlaenge,Konturmax);
    Kante = B{k};
    x= Kante(:,2);
    y= Kante(:,1);
    Flaecheninhalt = 0; Umfang = 0;
    %%%f�r c)
    N=size(Kante,1);
    for j = 1:N-1,
         Flaecheninhalt = Flaecheninhalt + x(j)*y(j+1) - x(j+1)*y(j);  %Gau�sche Trapezformel f�r A eines Polygon
         Umfang          = Umfang          + ((x(j+1)-x(j))^2 + (y(j+1)-y(j))^2)^0.5; %aus Satz d Pythagoras
    end 
    Flaecheninhalt = (Flaecheninhalt +  x(N)*y(1) - x(1)*y(N))/2;       %N-te Element bereitet sonst Probleme und wird aus der Summer herausgezogen A muss noch halbiert werden
    Umfang =           Umfang          +  ((x(1)-x(N))^2 + (y(1)-y(N))^2)^0.5;
    Kreishaftigkeit = 4*pi*Flaecheninhalt/Umfang^2;
    Merkmalsvektor = [Umfang,Kreishaftigkeit];
    X(i-2,:) = Merkmalsvektor; 
    %%%
    subplot(4,3,i-2)
    imshow(img,[])
    hold on 
    plot(x,y,'b','LineWidth',1.5)
    title(num2str(i-2))
end 
%%weiter f�r c)
[idx,c] = kmeans(X,2); %idx: cluster index , c: zentroidkoordinaten
%%%%%%%%%%%%%% Darstellen des erstellten Clusters d)
figure(3)
plot(X(idx==1,1),X(idx==1,2),'r.','MarkerSize',12) %vgl doc kmeans example
hold on
plot(X(idx==2,1),X(idx==2,2),'g.','MarkerSize',12)
plot(c(:,1),c(:,2),'b.',...
     'MarkerSize',12,'LineWidth',2)
 xlabel('Umfang[pixel]')
 ylabel('Kreishaftigkeit')
 legend('Cluster1: Muttern','Cluster2: Schrauben','Zentroiden')
 %%%%%%%%%%%%%%%%%%%%%%%
figure(4)
for i = 3:14,
    img = dbv_imageRead(Bilder(i).name,'gray'); %Bilder(i).name erlaubt Zugriff auf den i-ten Dateinamen
    img = edge(img,'sobel','nothinning');
    img = imclose(img,glaette7);
    [B,L] = bwboundaries(img,'noholes');
        Konturlaenge = zeros(1,size(B,1));             %Initialisiere Vektor, der die L�ngen speichert
    for j=1:size(B,1)
        Konturlaenge(j) = size(B{j},1);                % Greife auf die jeweilige Kantenl�nge zu
    end  
    Konturmax = max(Konturlaenge);
    k = find(Konturlaenge,Konturmax);
    Kante = B{k};
    x= Kante(:,2);
    y= Kante(:,1);
   subplot(4,3,i-2)
    imshow(img,[])
    hold on 
    if idx(i-2) == 1
    plot(x,y,'r','LineWidth',1.5)
    elseif idx(i-2) ==2 
    plot(x,y,'g','LineWidth',1.5)
    end 
    title(num2str(i-2))
end 

%%%%