function aufgabe2(filename, filtertype)
%%%%%%%%%%%%%%%%%%%%%%%%
% a) 

%Bild laden                                     %in Variable imgOriginal
imgOriginal = dbv_imageRead(filename, 'gray');
E=zeros(5,3);   %Matrix f�r Abweichungswerte anlegen

% verschiedene Filter durchlaufen: 
figure(1)                               % erzeuge neue Figureumgebung
set(gcf,'Name',filtertype)              % Benenne diese nach dem Filter
subplot(1,4,1)                          % Anordnen der Bilder
imshow(imgOriginal)                     % Original
title('Original')           
j=1;
for i=3:2:7,                            % diese Fenstergr��en werden benutzt
    imgFiltered = dbv_filterImage(imgOriginal, filtertype, i); % Bild wird gefiltert
    subplot(1,4,j+1)                    % schreibe in diesen subplot
    imshow(imgFiltered)     
    title(['Fenster' num2str(i)])       % erzeuge Titel �berm Bild
    j=j+1;
end %for i

%%%%%%%%%%%%%%%%%%%%%%%%
%b)

%Hinzuf�gen von Rauschen
                                      

k=1;                                             % die Laufvariable k hilft bei dem durchnummerieren der Position im subplot
for i = 1:5                                     % f�nf unterschiedliche Standardabweichungen
    figure(2)                                   %Ziegt das Originalbild+Rauschen in einer Figure an
    set(gcf,'Name',[filtertype, ' +Rauschen'])  % Benennen der Figure
    sigma_noise = 1/(2*i);                      % Legt die Standardabweichung fest
    imgNoise = dbv_addNoise(imgOriginal, 'gaussian', sigma_noise); % erzeuge verrauschtes Bild
    subplot(5,1,i)                              % Bildet alle verrauschten Bilder in einer Figure ab
    imshow(imgNoise)
    title(['Rausch;\sigma=1/(2*' num2str(i) ')'])        % Titel dient der �bersicht
    
    for j = 3:2:7                               % Fenstergr��en {3,5,7}
        figure(3)                               % neue Figure
        set(gcf,'Name',['mit ', filtertype, ' gefiltert']) % 
        imgFiltered = dbv_filterImage(imgNoise, filtertype, j); % Bild wird gefiltert
        subplot(5,3,k)                    % stellt die gefilterten Bilder tabellarisch dar
        imshow(imgFiltered)     
        title(['Fenster' num2str(j)])       % erzeuge Titel �berm Bild
        %einbauen von c) 
        if mod(k,3) == 0,
            l=3;
        else
            l = mod(k,3);
        end %if 
        E(i,l) = dbv_rms(imgOriginal, imgFiltered);
        k = k+1;
    end % for j
end %for i 
figure(4)
bar3(E)
disp(E)
figure(5)
bar(E,'grouped')