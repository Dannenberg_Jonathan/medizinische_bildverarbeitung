function filtered = dbv_filterImage(img, type, param)     % f�hrt verschiedene Filter durch aufs Bild, mit gegebener Fenstergr��e
switch type                                               % type gibt an welchen Filter  
    case 'average'                                        % average: Mittelwert-Tiefpass  
        h=fspecial('average', param);                     % fspecial erstellt eine Filtermatrix h
        filtered = imfilter(img, h, 'conv');              % imfilter wendet filter aufs Bild an    
    case 'gaussian'                                       % Gau�filter  
        h=fspecial('gaussian', param);
        filtered = imfilter(img, h, 'conv');
    case 'median'
        filtered = medfilt2(img,[param param]);           % medfilt2 wendet einen Medianfilter aufs Bild an   
end