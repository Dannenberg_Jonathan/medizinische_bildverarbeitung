function rms = dbv_rms(img1, img2)
N = size(img1,1)*size(img1,2);
diff = img1-img2;
quad = diff.^2;
summe = sum(quad(:));
summe_normiert = summe/N;
rms = summe_normiert.^0.5;

 