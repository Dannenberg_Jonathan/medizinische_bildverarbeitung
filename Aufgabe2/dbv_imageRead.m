function img = dbv_imageRead(filename, colormode)           %% liest ein Bild
img = imread(filename);
if (strcmp(colormode, 'gray'))
    img = rgb2gray(img);
end