function h=dbv_faltungOrt_1D(f, g)

%1. L�nge der Vektoren bestimmen
N=length(f);      %Signal der L�nge N
G=length(g);      %Filterkern der L�nge G

%2. Faltung im Ortsbereich OHNE Befehl "conv" oder "filter"!
%Randbehandlung: zum Beispiel f beiderseits um halbe Filterkernl�nge mit 0 erweitern
%auf Gesamtl�nge N+G-1, und Ergebnis h wieder auf L�nge N beschneiden
%Bemerkung: resultierende Faltung ist dadurch nicht kommutativ!

r = fix(G/2);                           %halbe Filterkernl�nge  
fExt =	[zeros(1,r),f,zeros(1,r)];      %erweiterter Eingabevektor
h = zeros(1,N+G-1);						%Ausgabevektor anlegen

for i=1+r:length(fExt)-r                %Bestimme alle relevanten i-ten Stellen des Ausgabevektors (welche nicht eh wieder abgeschnitten werden)
    %Schleifenrumpf
    for j = 1:G                         % Schiebe Filter 'unterm' Signal durch 
         h(i)=h(i)+ fExt(i+1+j-G)*g(end+1-j); 
                                           %  Arg von f wird jedesmal
                                           %  erh�ht und Arg von g
                                           %  verkleinert -> f(i+j) bzw
                                           %  g(end-j) , dann noch auf die
                                           %  Gr��e des Filterkerns G
                                           %  angepasst -> f(i+j-G) , und
                                           %  Matlab z�hlt die Indizes von
                                           %  Vektoren ab 1 -> f(i+j-G+1)
                                           %  bzw g(end-j+1)
                                           %  Bsp: h(2)=Summe[f(1)g(3)+f(2)g(2)+f(3)g(1)]
    end
end

%3. Erweiterungen entfernen
h = h(1+r:end-r);                           %auf beiden Seiten wird der halbe Filterkern abgeschnitten