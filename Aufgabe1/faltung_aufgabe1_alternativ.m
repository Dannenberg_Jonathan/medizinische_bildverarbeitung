function h = faltung_aufgabe1_alternativ(f,g)

%%% L�ngen
m = length(f);
n = length(g);

%%% Dimensioniere h
h=zeros(1,m+n-1);
%%% Berechnung der diskreten Faltung   (definiert als : f**g(n)=sum_k(f(k)*g(n-k))

for k = 1:m+n-1,                      
    for j = max(1,k+1-n):min(k,m),
     h(k) = h(k)+ f(j)*g(k-j+1);       %%% Summe berechnen und dem kten Element des Ausgabevektors zuordnen
    end % for j    
end % for i

