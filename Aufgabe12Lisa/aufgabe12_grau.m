function aufgabe12_grau( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%a)
%5x5
a = dbv_imageRead(filename, 'gray'); %Bild wird eingelesen
figure(1)
imshow(a)
title('Originalbild')

figure(2)
b=strel('square',5);
c=imclose(a,b);
subplot(3,4,1)
imshow(c)
title(['Grauwert-Closing', num2str(5)])
subplot(3,4,2)
imhist(c)
title(['Grauwert-Closing Histogramm', num2str(5)])
d=imopen(a,b);
subplot(3,4,3)
imshow(d)
title(['Grauwert-Opening' num2str(5)])
subplot(3,4,4)
imhist(d)
title(['Grauwert-Opening Histogramm', num2str(5)])

%7x7
e=strel('square',7);
f=imclose(a,e);
subplot(3,4,5)
imshow(f)
title(['Grauwert-Closing' num2str(7)])
subplot(3,4,6)
imhist(f)
title(['Grauwert-Closing Histogramm' num2str(7)])
g=imopen(a,e);
subplot(3,4,7)
imshow(g)
title(['Grauwert-Opening' num2str(7)])
subplot(3,4,8)
imhist(g)
title(['Grauwert-Opening Histogramm' num2str(7)])

%b)

subplot(3,4,9)
h=imclose(c,b);
imshow(h)
title(['Grauwert-Closing wiederholt' num2str(5)])
subplot(3,4,10)
imhist(h)
title(['Grauwert-Closing wiederholt Histogramm' num2str(5)])
subplot(3,4,11)
i=imopen(d,b);
imshow(i)
title(['Grauwert-Opening wiederholt'  num2str(5)])
subplot(3,4,12)
imhist(i)
title(['Grauwert-Opening wiederholt Histogramm' num2str(5)])

%c) rechteckig 3x7 und 7x3
figure(3)
j= strel('rectangle', [3 7]);
k= strel('rectangle', [7 3]);
l=imclose(a,j);
subplot(2,2,1)
imshow(l)
title(['Grauwert-Closing' num2str(37)])
subplot(2,2,2)
imhist(l)
title(['Grauwert-Closing Histogramm' num2str(37)])
m=imopen(a,k);
subplot(2,2,3)
imshow(m)
title(['Grauwert-Opening'  num2str(73)])
subplot(2,2,4)
imhist(m)
title(['Grauwert-Opening Histogramm' num2str(37)])

%e)
%Bilder f und g Histogrammglättung
figure(4)
subplot(3,4,1)
imshow(f)
title(['Grauwert-Closing' num2str(7)])
subplot(3,4,2)
imhist(f)
title(['Grauwert-Opening Histogramm' num2str(7)])
subplot(3,4,3)
imshow(g)
title(['Grauwert-Opening' num2str(7)])
subplot(3,4,4)
imhist(g)
title(['Grauwert-Opening Histogramm' num2str(7)])
subplot(3,4,5)
imshow(histeq(f))
title(['Grauwert-Closing Histogrammglättung' num2str(7)])
subplot(3,4,6)
imhist(histeq(f))
subplot(3,4,7)
imshow(histeq(g))
title(['Grauwert-Opening Histogrammglättung' num2str(7)])
subplot(3,4,8)
imhist(histeq(g))
end

